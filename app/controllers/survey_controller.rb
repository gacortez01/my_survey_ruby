class SurveyController < ApplicationController
  def index
    @survey = Survey.new
  end
  def create
    @survey = Survey.new(name: params[:name], location: params[:location], language: params[:language], comment: params[:comment]) 
    if @survey.save
      flash[:success] = "Thanks for submitting this form. You have submitted this for No. times!"
      redirect_to "/result"
    else
      flash[:errors] = @survey.errors.full_messages
      redirect_to "/result"
    end
  end

  def increment_counter
    if session[:counter].nil?
      session[:counter] = 0
    end
    session[:counter] += 1
  end

  def show
    @count = increment_counter
    flash[:success] = "Thanks for submitting this form. You have submitted this for #{@count} times!"
    @survey = Survey.new
    @surveys = Survey.all
  end
end
